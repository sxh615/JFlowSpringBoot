package BP.WF.Port.SubInc;

import BP.DA.*;
import BP.En.*;
import BP.Web.*;
import BP.WF.*;
import BP.WF.Port.*;
import java.util.*;

/** 
 部门属性
*/
public class DeptAttr extends EntityNoNameAttr
{
	/** 
	 父节点编号
	*/
	public static final String ParentNo = "ParentNo";
}