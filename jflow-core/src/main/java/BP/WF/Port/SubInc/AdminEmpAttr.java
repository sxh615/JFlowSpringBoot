package BP.WF.Port.SubInc;

import BP.DA.*;
import BP.En.*;
import BP.WF.*;
import BP.Port.*;
import BP.Web.*;
import BP.WF.*;
import BP.WF.Port.*;
import java.util.*;

/** 
 管理员
*/
public class AdminEmpAttr
{

		///#region 基本属性
	/** 
	 No
	*/
	public static final String No = "No";
	/** 
	 申请人
	*/
	public static final String Name = "Name";
	public static final String LoginData = "LoginData";
	public static final String Tel = "Tel";
	/** 
	 授权人
	*/
	public static final String Author = "Author";
	/** 
	 授权日期
	*/
	public static final String AuthorDate = "AuthorDate";
	/** 
	 是否处于授权状态
	*/
	public static final String AuthorWay = "AuthorWay";
	/** 
	 授权自动收回日期
	*/
	public static final String AuthorToDate = "AuthorToDate";
	public static final String Email = "Email";
	public static final String AlertWay = "AlertWay";
	public static final String Stas = "Stas";
	public static final String Depts = "Depts";
	public static final String FK_Dept = "FK_Dept";
	/** 
	 所在组织
	*/
	public static final String OrgNo = "OrgNo";
	public static final String Idx = "Idx";
	public static final String FtpUrl = "FtpUrl";
	public static final String Style = "Style";
	public static final String Msg = "Msg";
	public static final String TM = "TM";
	public static final String UseSta = "UseSta";
	/** 
	 授权的人员
	*/
	public static final String AuthorFlows = "AuthorFlows";
	/** 
	 用户状态
	*/
	public static final String UserType = "UserType";
	/** 
	 流程根目录
	*/
	public static final String RootOfFlow = "RootOfFlow";
	/** 
	 表单根目录
	*/
	public static final String RootOfForm = "RootOfForm";
	/** 
	 部门根目录
	*/
	public static final String RootOfDept = "RootOfDept";

		///#endregion
}