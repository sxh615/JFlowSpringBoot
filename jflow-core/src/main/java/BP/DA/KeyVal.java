package BP.DA;

import java.util.*;

/** 
 字段
*/
public class KeyVal
{
	/** 
	 字段名称
	*/
	private String key;
	public final String getkey()
	{
		return key;
	}
	public final void setkey(String value)
	{
		key = value;
	}
	/** 
	 字段值
	*/
	private String value;
	public final String getvalue()
	{
		return value;
	}
	public final void setvalue(String value)
	{
		value = value;
	}
	/** 
	 类型
	*/
	private String type;
	public final String gettype()
	{
		return type;
	}
	public final void settype(String value)
	{
		type = value;
	}
}